from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import Todo_list_form, Todo_item_form

# Create your views here.
def todo_list_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todo_list_list": todolists,
    }
    return render(request, "todos/todo_lists.html" , context)

def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todolist,
    }
    return render(request, "todos/todo_list_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = Todo_list_form(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = Todo_list_form()
    context = {
        "form":form,
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = Todo_list_form(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = Todo_list_form(instance=todolist)
    context = {
        "form":form,
        "todo_list_detail":todolist
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = Todo_item_form(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = Todo_item_form()
    context = {
        "form":form,
    }
    return render(request, "todos/add.html", context)

def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = Todo_item_form(request.POST, instance=todoitem)
        if form.is_valid():
            item=form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = Todo_item_form(instance=todoitem)
    context = {
        "todo_item":todoitem,
        "form":form,
    }
    return render(request, "todos/item_update.html", context)
